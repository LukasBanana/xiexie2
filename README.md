# XieXie 2.0 Compiler and VirtualMachine #

### Features ###

* High Level Programming Language
* Compiler (in progress)
* Virtual Assembler (XASM)
* Virtual Machine (written in plain C with C++ wrapper)

### License ###

BSD License
