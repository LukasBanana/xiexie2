/*
 * Parser header
 * 
 * This file is part of the "XieXie 2.0 Project" (Copyright (c) 2014 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_PARSER_H__
#define __XX_PARSER_H__


#include <string>


namespace SyntaxAnalyzer
{


//! Syntax parser class.
class Parser
{
    
    public:
        
        Parser() = default;


    private:
        

};


} // /namespace SyntaxAnalyzer


#endif



// ================================================================================